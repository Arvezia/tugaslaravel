<?php

use App\Http\Controllers\CastController;
use Illuminate\Support\Facades\Route;
use PhpParser\Node\Expr\Cast;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// CRUD Cast
// Create
Route::get('/cast/create',[CastController::class, 'create']);
// Store -> Database
Route::post('/cast',[CastController::class, 'store']);
// Read -> Display Data
Route::get('/cast',[CastController::class, 'index']);
// Detail Based On ID
Route::get('/cast/{cast_id}', [CastController::class, 'show']);
// Update -> edit
route::get('/cast/{cast_id}/edit',[CastController::class, 'edit']);
// Insert to database
route::put('/cast/{cast_id}', [CastController::class, 'update']);
// Delete
route::delete('/cast/{cast_id}', [CastController::class, 'destroy']);