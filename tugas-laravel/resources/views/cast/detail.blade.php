@extends('layout.master')

@section('title')
    Detail Page
@endsection

@section('content')
<h1 class="text-primary">{{$cast->nama}} ({{$cast->umur}})</h1>
{{-- <p>Umur: {{$cast->umur}}</p> --}}
<p>{{$cast->bio}}</p>
<a href="/cast" class="btn btn-primary btn-sm mb-3 ">Back</a>
@endsection