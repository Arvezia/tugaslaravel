@extends('layout.master')

@section('title')
    Create Page
@endsection

@section('content')
<form action="/cast" method="POST">
{{-- Nama --}}
    @csrf
    <div class="form-group">
      <label>Nama Cast</label>
      <input type="text" name="nama" class="form-control">
    </div>
{{-- Error Nama --}}
@error('nama')
<div class="alert alert-danger">{{ $message }}</div>
@enderror
{{-- Umur --}}
    <div class="form-group">
        <label>Umur</label>
        <input type="number" name="umur" class="form-control">
      </div>
{{-- Error Umur --}}
@error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
{{-- Bio --}}
    <div class="form-group">
      <label>Bio</label>
      <textarea name="bio" class="form-control" cols="30" rows="10"></textarea>
    </div>
{{-- Error Bio --}}
@error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection