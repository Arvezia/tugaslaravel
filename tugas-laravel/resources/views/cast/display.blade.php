@extends('layout.master')

@section('title')
    Display Page
@endsection

@section('content')
{{-- Create Button --}}
<a href="/cast/create" class="btn btn-primary btn-sm mb-3 "> Create</a>
{{-- Table --}}
 <table class="table">
    <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Nama</th>
            <th scope="col">Action</th>
            {{-- <th scope="col">Umur</th>
            <th scope="col">Bio</th> --}}
        </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key => $value)
            <tr>
                <td>{{$key +1}}</td>
                <td>{{$value->nama}}</td>
                {{-- <td><{{$value->umur}}/td>
                <td>{{$value->Bio}}</td> --}}
                <td>
                    <form action="/cast/{{$value->id}}" method="POST">
                    <a href="/cast/{{$value->id}}" class="btn btn-primary btn-sm"> Detail</a>
                    <a href="/cast/{{$value->id}}/edit" class="btn btn-warning btn-sm "> Edit</a>
                    @csrf
                    @method('delete')
                    <input type="submit" value="Delete" class="btn btn-danger btn-sm" >
                    </form>
                </td>
                
            </tr>
        @empty
            <tr>
                <td> No Cast</td>
            </tr>
        @endforelse
    </tbody>
 </table>
@endsection