<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB; 

class CastController extends Controller
{
// Create function
    public function create()
    {
        return view ('cast.create');
    }
// Storing Function
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);

        DB::table('cast')->insert([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['bio']
        ]);

        return redirect('/cast');
    }
// Display Function
    public function index (){
        $casts =DB::table('cast')->get();
        // dd($casts);
 
        return view('cast.display', ['cast' => $casts]);
    
    }
// Detail Function
    public function show ($id)
    {
        $casts = DB::table('cast')->where('id', $id)->first();
        return view('cast.detail', ['cast'=>$casts]);
    }
// Edit Function
    public function edit ($id)
    {
        $casts = DB::table('cast')->where('id', $id)->first();
        return view('cast.edit', ['cast'=>$casts]);
    }
// Update after edit
    public function update (Request $request,$id)
    {
        DB::table('cast')
              ->where('id', $id)
              ->update([
                'nama' => $request['nama'],
                'umur' => $request['umur'],
                'bio' => $request['bio'],
            ]);
            return redirect('/cast');
    }
// Delete Function
    public function destroy($id)
    {
        DB::table('cast')->where('id', '=', $id)->delete();
        return redirect('/cast');
    }
}
